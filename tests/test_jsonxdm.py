
import os, jsonxdm

import json, jsonxdm

from jsonxdm.jsonxdm import loads as jxl
from jsonxdm.jsonxdm import dumps as jxd
from lxml.etree import tostring

thisdir = os.path.dirname(__file__)
testinputdatadir = os.path.join(thisdir,'input')
testoutputdatadir = os.path.join(thisdir,'output')


with open(os.path.join(testinputdatadir,'sample.json')) as fd:
    jsoninput = fd.read()


class TestJSONXDM():

    def test_one(self):
        x = jxl(jsoninput)
        xs = tostring(x, pretty_print=True)
        with open(os.path.join(testoutputdatadir, 'output.xml'),'wb') as fd:
            fd.write(xs)

    def test_two(self):
        x = jxl(jsoninput)
        d = jxd(x, indent=3)
        with open(os.path.join(testoutputdatadir, 'output.json'),'wb') as fd:
            fd.write(d.encode('utf-8'))


